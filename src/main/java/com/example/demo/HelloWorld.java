package com.example.demo;

public class HelloWorld {
    private String desc;
    private String who;

    public HelloWorld(String who) {
        this.desc = "Hi " + who;
        this.who = who;
    }
}
